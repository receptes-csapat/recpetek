<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="registerModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body">

                <form class="text-center" action=""> 
                    <p class="h4 mb-3">Regisztráció</p>
                    <input type="text" id="registerFormName" class="form-control mb-3" placeholder="Név">
                    <input type="text" id="registerFormUsername" class="form-control mb-3" placeholder="Felhasználónév">
                    <input type="password" id="registerFormPassword" class="form-control mb-3" placeholder="Jelszó">
                    <input type="password" id="registerFormPasswordConfirmation" class="form-control mb-3" placeholder="Jelszó megerősítése">
                    <div class="d-grid">
                        <button class="btn btn-primary mb-3" type="submit" id="login">Regisztráció</button>
                    </div>
                    <p>Már regisztráltál? <a id="registerFormGoToLogin" href="#loginModal">Bejelentkezés</a></p>
                    <script>
                        $("#registerFormGoToLogin").on("click", function ()
                        {
                            function showLoginModal()
                            {
                                $("#loginModal").modal("show");
                                $("#registerModal").off("hidden.bs.modal", showLoginModal);
                            }
                            $("#registerModal").on("hidden.bs.modal", showLoginModal);
                            $("#registerModal").modal("hide");
                        });
                    </script>
                </form>

            </div>
        </div>
    </div>
</div>
