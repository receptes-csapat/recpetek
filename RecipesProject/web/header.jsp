<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@include file = "login.jsp" %>
<%@include file = "register.jsp" %>
<%@include file = "settings.jsp" %>
<%@include file = "functions.jsp" %>

<header class="section-header">
    <div class="container">
        <div class="row align-items-center">
            <div class="col py-4">
                <h2>Logo</h2>
            </div>
            <div class="col-auto py-2">
                <% if (session.getAttribute("isLoggedIn") != null && (Boolean) (session.getAttribute("isLoggedIn"))) { %>
                <div class="dropdown">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuProfile" data-bs-toggle="dropdown">Profil</a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuProfile" style="z-index: 42000">
                        <li>
                            <span class="dropdown-item-text">
                                <%
                                    if (session.getAttribute("user") != null) {
                                        out.print(((User) session.getAttribute("user")).getName());
                                    }
                                %>
                            </span>
                        </li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#settingsModal" data-bs-toggle="modal" data-bs-target="#settingsModal">Adatok módosítása</a></li>
                        <li><a class="dropdown-item" href="javascript:logout()">Kijelentkezés</a></li>
                        <script>
                            function logout() {
                                var data = {"task": "logout"};
                                $.ajax({
                                    type: "POST",
                                    url: "",
                                    data: data,
                                    success: function () {
                                        location.reload();
                                    },
                                    error: function () {}
                                });
                            }
                        </script>
                    </ul>
                </div>
                <% } else { %>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#loginModal">Bejelentkezés</button>
                <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#registerModal">Regisztráció</button>
                <% } %>
            </div>
        </div>
    </div>
</header>

<nav class="navbar sticky-top navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarToggler">
            <div class="navbar-nav">
                <% if (session.getAttribute("isLoggedIn") != null && (Boolean) (session.getAttribute("isLoggedIn"))) { %>
                <a class="nav-link" href="?page=favs" ">Kedvencek</a>
                <% }%>
                <a class="nav-link" href="?page=recipes" ">Receptek</a>
                <a class="nav-link" href="?page=categories" ">Kategóriák</a>
                <a class="nav-link" href="?page=ingredients" ">Alapanyagok</a>
            </div>
        </div>
        <form class="form-inline">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><span><i class="fas fa-search"></i></span></span>
                </div>
                <input class="form-control" type="text" placeholder="Search">
            </div>
        </form>
    </div>
</nav>
