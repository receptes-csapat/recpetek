<%@page import="java.io.File"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>



<div class="container">
    <%
        String[] imagePaths = (String[])(session.getAttribute("imagePaths"));
        String[] refs = {};
        for (String imagePath : imagePaths) {
            
           // style-t ki kéne tenni CSS-be
           // out.println("<img src='" + imagePath + "' style='object-fit: cover; object-position: center center; width: 200px; height: 200px; padding: 1px;'>");
           // refs[i] = imagePath;
        }
        //képek teszteléséhez
        int n = 0;   //dbszám
        //page
        int p = 1; //oldalszám
        if (request.getParameter("pages") != null){
            if ("1".equals(request.getParameter("pages"))){
                n = 0;
            }else {
             n = (Integer.parseInt(request.getParameter("pages"))-1)*4;   
            }
            p = Integer.parseInt(request.getParameter("pages"));
        }
        
        int max = imagePaths.length; //max darabszám
        
        
    %>
    

    <div class="row">
      <div class="col-md-8">
        <div class="pb-3">
            <h2 class="text-center mt-3">KEDVENCEK</h2>
        </div>
        <!-- DECK -->
        <div class="row">
            <!-- CARD HOLDER-->
            <div class="col-md-3">
                .col-md-3 #<%out.print(n+1);%>
                <!-- CARD -->
                <div class="card">
                    <%
                      //TESZTHEZ
                      out.println("<img src='" + imagePaths[n] + "' class='card-img-top' alt='alt'>");
                      if (imagePaths.length > n+1) n++;                       
                    %>
                    <div class="card-body">
                        <h5 class="card-title">Kedvenc #<%out.print(n);%></h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <a href="?page=1" class="btn btn-primary stretched-link m-3">Megtekintés</a>
                </div>
            </div>
            <!-- CARD HOLDER-->
            <div class="col-md-3">
                .col-md-3 #<%out.print(n+1);%>
                <!-- CARD -->
                <div class="card">
                    <%
                      //TESZTHEZ
                      out.println("<img src='" + imagePaths[n] + "' class='card-img-top' alt='alt'>");
                      if (imagePaths.length > n+1) n++;                       
                    %>
                    <div class="card-body">
                        <h5 class="card-title">Kedvenc #<%out.print(n);%></h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <a href="#" class="btn btn-primary stretched-link m-3">Megtekintés</a>
                </div>
            </div>
            <!-- CARD HOLDER-->
            <div class="col-md-3">
                .col-md-3 #<%out.print(n+1);%>
                <!-- CARD -->
                <div class="card">
                    <%
                      //TESZTHEZ
                      out.println("<img src='" + imagePaths[n] + "' class='card-img-top' alt='alt'>");
                      if (imagePaths.length > n+1) n++;                       
                    %>
                    <div class="card-body">
                        <h5 class="card-title">Kedvenc #<%out.print(n);%></h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <a href="#" class="btn btn-primary stretched-link m-3">Megtekintés</a>
                </div>
            </div>
            <!-- CARD HOLDER-->
            <div class="col-md-3">
                .col-md-3 #<%out.print(n+1);%>
                <!-- CARD -->
                <div class="card">
                    <%
                      //TESZTHEZ
                      out.println("<img src='" + imagePaths[n] + "' class='card-img-top' alt='alt'>");
                      if (imagePaths.length > n+1) n++;                       
                    %>
                    <div class="card-body">
                        <h5 class="card-title">Kedvenc #<%out.print(n);%></h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        
                    </div>
                    <a href="#" class="btn btn-primary stretched-link m-3">Megtekintés</a>
                </div>
            </div>
        </div>
    </div>
        <div class="col-md-3 mt-4 pt-5">
            .col-md-3 #menu
            <span class="border"></span>
            <div class="container border">
                <div class="d-grid gap-5 col-8 mx-auto mt-5 pt-1 mb-5 pb-1">
                    <!-- TODO: INTERACTIVE BUTTONS  -->
                    <button class="btn btn-primary" type="button">Reggeli</button>
                    <button class="btn btn-primary" type="button">Tízórai</button>
                    <button class="btn btn-primary" type="button">Ebéd</button>
                    <button class="btn btn-primary" type="button">Uzsonna</button>
                    <button class="btn btn-primary" type="button">Vacsora</button>
                    <button class="btn btn-primary" type="button">Desszert</button>
                </div>
            </div>
        </div>
    </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-6 m-2 p-2">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="?page=favs&pages=<% if((p-1) != 0){ out.print(p-1);} else {out.print(1);} %>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <!-- PAGINATION TODO: BUGFIX NEXT/PREVIOUS -->
                    <li class="page-item"><a class="page-link" href="?page=favs&pages=1" onclick="">1  </a></li>
                    <li class="page-item"><a class="page-link" href="?page=favs&pages=2" onclick="">2  </a></li>
                    <li class="page-item"><a class="page-link" href="?page=favs&pages=3" onclick="">3 </a></li>
                    <li class="page-item">
                        <a class="page-link" href="?page=favs&pages=<% if((p*4) < max ){out.print(p+1);} else {out.print(p);} %>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
            </div>
        </div>
</div>
 




                    
 
    <p></p>
    <p></p>
    <p>A Á B C D E É F G H I Í J K L M N O Ó Ö Ő P Q R S T U Ú V W X Y Z</p>
    <p>a á b c d e é f g h i í j k l m n o ó ö ő p q r s t u ú v w x y z</p>
    <p>A</p>
    <p>B</p>
    <p>C</p>
    <p>D</p>
    <p>E</p>
    <p>F</p>
    <p>A</p>
    <p>B</p>
    <p>C</p>
    <p>D</p>
    <p>E</p>
    <p>F</p>
    <p>A</p>
    <p>B</p>
    <p>C</p>
    <p>D</p>
    <p>E</p>
    <p>F</p>
    <p>A</p>
    <p>B</p>
    <p>C</p>
    <p>D</p>
    <p>E</p>
    <p>F</p>
    <p>A</p>
    <p>B</p>
    <p>C</p>
    <p>D</p>
    <p>E</p>
    <p>F</p>
    <p>A</p>
    <p>B</p>
    <p>C</p>
    <p>D</p>
    <p>E</p>
    <p>F</p>
    <p>A</p>
    <p>B</p>
    <p>C</p>
    <p>D</p>
    <p>E</p>
    <p>F</p>
