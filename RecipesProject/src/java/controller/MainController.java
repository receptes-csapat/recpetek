/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import model.Recipe;
import model.User;
import service.EntityService;
import service.UserService;


/**
 *
 * @author tovtam
 */
public class MainController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html; charset=UTF-8");
        ServletContext sc = getServletContext();
        HttpSession session = request.getSession(true);
        try (PrintWriter out = response.getWriter()) {
            
            if (session.getAttribute("isLoggedIn") == null)
                session.setAttribute("isLoggedIn", false);
            
            // Bejelenetkezés POST kérés feldolgozása
            if (request.getParameter("task") != null && request.getParameter("task").equals("login")) {
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                UserService userService = new UserService();
                User user = userService.getUserByUsername(username);
                if (user != null && user.getPassword().equals(password)) {
                    // Sikeres bejelentkezés
                    session.setAttribute("isLoggedIn", true);
                    session.setAttribute("user", user);
                } else {
                    // Sikertelen bejelentkezés
                    session.setAttribute("isLoggedIn", false);
                }
                userService.close();
            }
            
            // Kijelenetkezés POST kérés feldolgozása
            if (request.getParameter("task") != null && request.getParameter("task").equals("logout")) {
                session.setAttribute("isLoggedIn", false);
            }
            
            sc.getRequestDispatcher("/head.jsp").include(request, response);
            sc.getRequestDispatcher("/header.jsp").include(request, response);
            
            // Képek a tile cucchoz
            String[] imagePaths = new File(getServletContext().getRealPath("/") + "/files/images").list();
            for (int i = 0; i < imagePaths.length; i++)
                imagePaths[i] = "files/images/" + imagePaths[i];
            session.setAttribute("imagePaths", imagePaths);
            
            String pageParameter = request.getParameter("page");
            //.sendRedirect("?page=" + pageParameter);

            //cardview content         
            if (pageParameter != null) {
                if ("recipes".equals(request.getParameter("page"))) {
                    sc.getRequestDispatcher("/content_recipes.jsp").include(request, response);
                } else if ("favs".equals(pageParameter)) {
                    sc.getRequestDispatcher("/content_fav.jsp").include(request, response);
                } else if ("categories".equals(pageParameter)) {
                    sc.getRequestDispatcher("/content_categories.jsp").include(request, response);
                }else if ("ingredients".equals(pageParameter)) {
                    sc.getRequestDispatcher("/content_ingredients.jsp").include(request, response);
                }
            }
            
            sc.getRequestDispatcher("/end.jsp").include(request, response);
        }
    }
    
    //register
    
   /* public class guru_register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
        @Override
     protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("registerFormName");
		String userName = request.getParameter("registerFormName");
		String passord = request.getParameter("registerFormPassword");
		String passwordConfirm = request.getParameter("registerFormPasswordConfirmation");
		
		if(name.isEmpty() || userName.isEmpty() || passord.isEmpty() || 
				passwordConfirm.isEmpty())
		{
			RequestDispatcher req = request.getRequestDispatcher("register_1.jsp");
			req.include(request, response);
		}
		else
		{
			RequestDispatcher req = request.getRequestDispatcher("register_2.jsp");
			req.forward(request, response);
		}
	}
}*/
    
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
