/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tovtam
 */
@Entity
@Table(name = "food")
@NamedQueries({
    @NamedQuery(name = "Recipe.findAll", query = "SELECT r FROM Recipe r")})
public class Recipe implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "name")
    private String name;
    
    @Size(max = 500)
    @Column(name = "photo")
    private String imagePath;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "portion")
    private Integer servings;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "seen")
    private Integer views;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "public")
    private Boolean isPublic;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "minutes_to_make")
    private Integer minutesToMake;
    
    @ManyToMany(mappedBy = "favourites")
    private Set<User> users;
    
    @JoinTable(name = "allergen_in_food", joinColumns = {
        @JoinColumn(name = "allergen_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "food_id", referencedColumnName = "id")})
    @ManyToMany
    private Set<Allergen> allergens;
    
    @JoinTable(name = "season_to_food", joinColumns = {
        @JoinColumn(name = "season_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "food_id", referencedColumnName = "id")})
    @ManyToMany
    private Set<Season> seasons;
    
    @JoinTable(name = "food_to_time", joinColumns = {
        @JoinColumn(name = "food_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "time_id", referencedColumnName = "id")})
    @ManyToMany
    private Set<Meal> meals;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
    private Set<Instruction> instructions;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
    private Set<Item> itemSet;
    
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Category category;
    
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Course course;
    
    @JoinColumn(name = "difficulty_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Difficulty difficulty;
    
    @JoinColumn(name = "price_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cost cost;
    
    @JoinColumn(name = "uploader_id", referencedColumnName = "id")
    @ManyToOne
    private User uploader;

    public Recipe() {
    }

    public Recipe(Integer id) {
        this.id = id;
    }

    public Recipe(Integer id, String name, Integer servings, Integer views, Date created, Boolean isPublic, Integer minutesToMake) {
        this.id = id;
        this.name = name;
        this.servings = servings;
        this.views = views;
        this.created = created;
        this.isPublic = isPublic;
        this.minutesToMake = minutesToMake;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Integer getMinutesToMake() {
        return minutesToMake;
    }

    public void setMinutesToMake(Integer minutesToMake) {
        this.minutesToMake = minutesToMake;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Allergen> getAllergens() {
        return allergens;
    }

    public void setAllergens(Set<Allergen> allergens) {
        this.allergens = allergens;
    }

    public Set<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(Set<Season> seasons) {
        this.seasons = seasons;
    }

    public Set<Meal> getMeals() {
        return meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }

    public Set<Instruction> getInstructions() {
        return instructions;
    }

    public void setInstructions(Set<Instruction> instructions) {
        this.instructions = instructions;
    }

    public Set<Item> getItemSet() {
        return itemSet;
    }

    public void setItemSet(Set<Item> itemSet) {
        this.itemSet = itemSet;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Cost getCost() {
        return cost;
    }

    public void setCost(Cost cost) {
        this.cost = cost;
    }

    public User getUploader() {
        return uploader;
    }

    public void setUploader(User uploader) {
        this.uploader = uploader;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recipe)) {
            return false;
        }
        Recipe other = (Recipe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Recipe[ id=" + id + " ]";
    }
    
}
