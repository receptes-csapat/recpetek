package service;

import javax.persistence.TypedQuery;
import model.User;

public class UserService extends EntityService {

    public UserService() {
    }

    public User getUserByUsername(String username) {
        if (!isClosed) {
            try {
                TypedQuery<User> query = entityManager.createQuery("SELECT t FROM User t WHERE t.username = :username", User.class);
                query.setParameter("username", username);
                return query.getSingleResult();
            } catch (Exception e) {
            }
        }
        return null;
    }

}
