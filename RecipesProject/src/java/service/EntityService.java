package service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class EntityService {

    protected EntityManagerFactory entityManagerFactory;
    protected EntityManager entityManager;
    protected Boolean isClosed;

    public EntityService() {
        entityManagerFactory = Persistence.createEntityManagerFactory("RecipesProjectPU");
        entityManager = entityManagerFactory.createEntityManager();
        isClosed = false;
    }

    public Boolean begin() {
        if (!isClosed) {
            try {
                if (!entityManager.getTransaction().isActive())
                    entityManager.getTransaction().begin();
                return true;
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
        return false;
    }
    
    public Boolean end() {
        if (!isClosed) {
            try {
                if (entityManager.getTransaction().isActive())
                    entityManager.getTransaction().commit();
                return true;
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                System.err.println(e.toString());
            }
        }
        return false;
    }
    
    public Boolean create(Object entity) {
        if (!isClosed) {
            try {
                if (entityManager.getTransaction().isActive())
                    return false;
                entityManager.getTransaction().begin();
                entityManager.persist(entity);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                System.err.println(e.toString());
            }
        }
        return false;
    }

    public Boolean remove(Object entity) {
        if (!isClosed) {
            try {
                if (entityManager.getTransaction().isActive())
                    return false;
                entityManager.getTransaction().begin();
                entityManager.remove(entity);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                System.err.println(e.toString());
            }
        }
        return false;
    }

    public <T> List<T> getAll(Class<T> entityClass) {
        return getAll(entityClass, Integer.MAX_VALUE, 0);
    }

    public <T> List<T> getAll(Class<T> entityClass, Integer count) {
        return getAll(entityClass, count, 0);
    }

    public <T> List<T> getAll(Class<T> entityClass, Integer count, Integer startingIndex) {
        if (!isClosed) {
            try {
                TypedQuery<T> query = entityManager.createQuery("SELECT t FROM " + entityClass.getName() + " t", entityClass);
                query.setFirstResult(startingIndex);
                query.setMaxResults(count);
                return query.getResultList();
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
        return null;
    }

    public Boolean close() {
        if (!isClosed) {
            entityManager.close();
            entityManagerFactory.close();
            isClosed = true;
            return true;
        }
        return false;
    }

}
