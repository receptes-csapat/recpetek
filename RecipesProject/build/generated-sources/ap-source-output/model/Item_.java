package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Ingredient;
import model.Recipe;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-12-22T13:11:12")
@StaticMetamodel(Item.class)
public class Item_ { 

    public static volatile SingularAttribute<Item, String> unit;
    public static volatile SingularAttribute<Item, Double> quantity;
    public static volatile SingularAttribute<Item, Ingredient> ingredient;
    public static volatile SingularAttribute<Item, Recipe> recipe;
    public static volatile SingularAttribute<Item, Integer> index;
    public static volatile SingularAttribute<Item, Integer> id;

}