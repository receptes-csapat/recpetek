package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Recipe;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-12-22T13:11:12")
@StaticMetamodel(Instruction.class)
public class Instruction_ { 

    public static volatile SingularAttribute<Instruction, Recipe> recipe;
    public static volatile SingularAttribute<Instruction, String> description;
    public static volatile SingularAttribute<Instruction, Integer> index;
    public static volatile SingularAttribute<Instruction, Integer> id;

}