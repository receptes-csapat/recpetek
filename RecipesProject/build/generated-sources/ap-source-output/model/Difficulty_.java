package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Recipe;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-12-22T13:11:12")
@StaticMetamodel(Difficulty.class)
public class Difficulty_ { 

    public static volatile SetAttribute<Difficulty, Recipe> recipes;
    public static volatile SingularAttribute<Difficulty, String> name;
    public static volatile SingularAttribute<Difficulty, Integer> id;

}