package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Allergen;
import model.Category;
import model.Cost;
import model.Course;
import model.Difficulty;
import model.Instruction;
import model.Item;
import model.Meal;
import model.Season;
import model.User;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-12-22T13:11:12")
@StaticMetamodel(Recipe.class)
public class Recipe_ { 

    public static volatile SetAttribute<Recipe, Instruction> instructions;
    public static volatile SetAttribute<Recipe, Season> seasons;
    public static volatile SingularAttribute<Recipe, Cost> cost;
    public static volatile SingularAttribute<Recipe, String> imagePath;
    public static volatile SingularAttribute<Recipe, Date> created;
    public static volatile SetAttribute<Recipe, Item> itemSet;
    public static volatile SetAttribute<Recipe, User> users;
    public static volatile SetAttribute<Recipe, Allergen> allergens;
    public static volatile SingularAttribute<Recipe, Difficulty> difficulty;
    public static volatile SingularAttribute<Recipe, Integer> minutesToMake;
    public static volatile SingularAttribute<Recipe, Integer> servings;
    public static volatile SingularAttribute<Recipe, User> uploader;
    public static volatile SingularAttribute<Recipe, String> name;
    public static volatile SingularAttribute<Recipe, Boolean> isPublic;
    public static volatile SingularAttribute<Recipe, Course> course;
    public static volatile SingularAttribute<Recipe, Integer> id;
    public static volatile SingularAttribute<Recipe, Category> category;
    public static volatile SingularAttribute<Recipe, Integer> views;
    public static volatile SetAttribute<Recipe, Meal> meals;

}