<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="loginModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body">

                <div class="text-center">
                    <p class="h4 mb-3">Bejelentkezés</p>
                    <input type="text" id="loginFormUsername" class="form-control mb-3" name="username" placeholder="Felhasználónév">
                    <input type="password" id="loginFormPassword" class="form-control mb-3" name="password" placeholder="Jelszó">
                    <div class="d-grid">
                        <button class="btn btn-primary mb-3" onclick="login()">Bejelentkezés</button>
                        <script>
                            function login() {
                                    var data = {"task": "login", "username": $('#loginFormUsername').val(), "password": $('#loginFormPassword').val() };
                                    $.ajax({
                                    type: "POST",
                                    url: "",
                                    data: data,
                                    success: function() { location.reload(); },
                                    error: function() {}
                                });
                            }
                        </script>
                    </div>
                    <p>Még nem regisztráltál? <a id="loginFormGoToRegister" href="#registerModal">Regisztráció</a></p>
                    <script>
                        $("#loginFormGoToRegister").on("click", function ()
                        {
                            function showRegisterModal()
                            {
                                $("#registerModal").modal("show");
                                $("#loginModal").off("hidden.bs.modal", showRegisterModal);
                            }
                            $("#loginModal").on("hidden.bs.modal", showRegisterModal);
                            $("#loginModal").modal("hide");
                        });
                    </script>
                </div>

            </div>
        </div>
    </div>
</div>
