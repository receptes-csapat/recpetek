<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="settingsModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body">

                <form class="text-center" action="">
                    <p class="h4 mb-3">Adatok módosítása</p>
                    <input type="text" id="settingsFormName" class="form-control mb-3" placeholder="Név"
                           <%
                                if (session.getAttribute("user") != null) {
                                    out.print("value='" + ((User)session.getAttribute("user")).getName() + "'");
                                }
                           %> >
                    <input type="text" id="settingsFormUsername" class="form-control mb-3" placeholder="Felhasználónév" disabled
                           <%
                                if (session.getAttribute("user") != null) {
                                    out.print("value='" + ((User)session.getAttribute("user")).getUsername()+ "'");
                                }
                           %> >
                    <input type="password" id="settingsFormCurrentPassword" class="form-control mb-3" placeholder="Jelenlegi jelszó">
                    <input type="password" id="settingsFormNewPassword" class="form-control mb-3" placeholder="Új jelszó">
                    <input type="password" id="settingsFormNewPasswordConfirmation" class="form-control mb-3" placeholder="Új jelszó megerősítése">
                    <div class="d-grid">
                        <button class="btn btn-primary mb-3" type="submit">Mentés</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
